#include <PID_v1.h>
// pendulum encoder pins
#define REF_OUT_A 18 // PD3 - đen
#define REF_OUT_B 19 // PD2 - trăng
#define PWM_PIN 10
#define DIR_PIN 8
#define OFFSET 60
#define DELAY1 250
#define DELAY2 500
const float setup_zero_speed =65.f;
// pulses per revolution
volatile long  refEncoderValue = 0;// 0-258//
volatile long lastRefEncoded = 0;
// pid var
double Input=0;
double sp=128.f;//pulse- 138
double Output=0;
//Specify the links and initial tuning parameters
double Kp=42,Ki=0,Kd=1;
PID myPID(&Input, &Output, &sp, Kp, Ki, Kd, DIRECT);
void driveMotor(double u);
void refEncoderHandler();
void stopMotor();
void setup() {
  // put your setup code here, to run once:
    Serial.begin(9600);
    TCCR2B = (TCCR2B & 0b11111000) | 0x01;
    pinMode(REF_OUT_A, INPUT_PULLUP);
    pinMode(REF_OUT_B, INPUT_PULLUP);
   pinMode(PWM_PIN, OUTPUT);
   pinMode(DIR_PIN, OUTPUT);
  attachInterrupt(digitalPinToInterrupt(REF_OUT_A), refEncoderHandler, CHANGE);
  attachInterrupt(digitalPinToInterrupt(REF_OUT_B), refEncoderHandler, CHANGE);
    myPID.SetMode(AUTOMATIC);
    myPID.SetSampleTime(1);
    myPID.SetOutputLimits(-255, 255);
}
void loop(){
Input=refEncoderValue;
while(Input<(sp+OFFSET)&&Input>(sp-OFFSET))
{
Input=refEncoderValue;
myPID.Compute();
driveMotor(float(-Output));
}
while(Input>(sp+OFFSET))
{
 driveMotor(-255.f);
 delay(DELAY1);
 driveMotor(255.f);
 delay(DELAY2);
 if(refEncoderValue<(sp+OFFSET))
 {
  stopMotor();
 }
  break;
 }
while(Input<(sp-OFFSET))
{
 driveMotor(255.f);
 delay(DELAY1);
 driveMotor(-255.f);
 delay(DELAY2);
 if(refEncoderValue>(sp-OFFSET))
 { stopMotor();
  break;
 }
}// swing from 0
}
 //Ham phuc vụ ngat Encoder
void refEncoderHandler() {
  int MSB = (PIND & (1 << PD3)) >> PD3; //MSB = most significant bit
  int LSB = (PIND & (1 << PD2)) >> PD2; //LSB = least significant bit
  int encoded = (MSB << 1) | LSB; //converting the 2 pin value to single number
  int sum  = (lastRefEncoded << 2) | encoded; //adding it to the previous encoded value

  if(sum == 0b1101 || sum == 0b0100 || sum == 0b0010 || sum == 0b1011) {
    refEncoderValue++; //CW
  }
  if(sum == 0b1110 || sum == 0b0111 || sum == 0b0001 || sum == 0b1000) {
    refEncoderValue--; //CCW
  }
  lastRefEncoded = encoded; //store this value for next time  
}
void driveMotor(float u) {
  if(u>=0){
     u = map(fabs(u), 0, 255, setup_zero_speed, 255);
    digitalWrite(DIR_PIN, LOW);
   }
  if(u<0){
    u = map(fabs(u), 0, 255, setup_zero_speed, 255);
   digitalWrite(DIR_PIN, HIGH); 
   u=255-fabs(u);
  }
analogWrite(PWM_PIN, fabs(u)); 
}
void stopMotor()
{  digitalWrite(DIR_PIN, LOW);
  analogWrite(PWM_PIN,0); 
}
